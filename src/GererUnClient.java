import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class GererUnClient implements Runnable{
    private Socket ss;

    public GererUnClient(Socket ss) {
        super();
        this.ss = ss;
    }
    public void run() {
        try {
            BufferedReader entreeSocket = new BufferedReader(new InputStreamReader(ss.getInputStream()));
            // Construction d'un PrintStream pour envoyer du texte à travers la connexion socket
            PrintStream sortieSocket = new PrintStream(ss.getOutputStream());

            String chaine = "";

            try {
                while(chaine != null) {
                    // lecture d'une chaine envoyée à travers la connexion socket
                    chaine = entreeSocket.readLine();

                    // si elle est nulle c'est que le client a fermé la connexion
                    if (chaine != null)
                        sortieSocket.println(chaine); // on envoie la chaine au client
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // on ferme nous aussi la connexion
            ss.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
