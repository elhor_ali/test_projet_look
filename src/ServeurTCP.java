import java.io.BufferedReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurTCP {
    private int port;

    public ServeurTCP(int port) {
        super();
        this.port = port;
    }
    public static void main(String args[]) throws Exception {
        int port = 40000;
        ServeurTCP serv = new ServeurTCP(port);

        serv.travail();


    }
    public void travail() {
        try {
            // Création d'un socket serveur générique sur le port 40000
            ServerSocket ssg = new ServerSocket(port);

            while(true) {
                // On attend une connexion puis on l'accepte

                Socket sss = ssg.accept();
                GererUnClient g=new GererUnClient(sss);
                Thread t = new Thread(g);
                t.start();


            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
